package ru.bokhan.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.bokhan.tm.api.ICurrentSessionService;
import ru.bokhan.tm.endpoint.SessionDTO;

@Getter
@Setter
@Service
public class CurrentSessionService implements ICurrentSessionService {

    @Nullable
    private SessionDTO currentSession;

}
