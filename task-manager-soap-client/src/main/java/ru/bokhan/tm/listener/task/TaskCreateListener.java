package ru.bokhan.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.api.service.ISessionService;
import ru.bokhan.tm.endpoint.soap.TaskDto;
import ru.bokhan.tm.endpoint.soap.TaskEndpoint;
import ru.bokhan.tm.event.ConsoleEvent;
import ru.bokhan.tm.listener.AbstractListener;
import ru.bokhan.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionService.insertTo(taskEndpoint);
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskDto taskDto = new TaskDto();
        taskDto.setName(name);
        taskDto.setDescription(description);
        taskDto.setProjectId(projectId);
        taskEndpoint.saveTask(taskDto);
        System.out.println("[OK]");
    }

}
