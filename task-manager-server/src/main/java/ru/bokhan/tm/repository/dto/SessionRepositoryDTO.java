package ru.bokhan.tm.repository.dto;

import ru.bokhan.tm.dto.SessionDTO;

public interface SessionRepositoryDTO extends AbstractRepositoryDTO<SessionDTO> {

}
