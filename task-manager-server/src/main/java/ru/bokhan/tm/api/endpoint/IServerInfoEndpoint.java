package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;

public interface IServerInfoEndpoint {

    @NotNull
    @WebMethod
    String getServerHost();

    @NotNull
    @WebMethod
    Integer getServerPort();

}
