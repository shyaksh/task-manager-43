package ru.bokhan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.bokhan.tm.api.service.ISessionService;

public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    ISessionService sessionService;

}
