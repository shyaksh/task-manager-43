package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bokhan.tm.api.endpoint.IDomainEndpoint;
import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    @Autowired
    IDomainService domainService;

    @Override
    @WebMethod
    public boolean saveToXml(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.saveToXml();
    }

    @Override
    @WebMethod
    public boolean loadFromXml(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.loadFromXml();
    }

    @Override
    @WebMethod
    public boolean removeXml(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.removeXml();
    }

    @Override
    @WebMethod
    public boolean saveToJson(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.saveToJson();
    }

    @Override
    @WebMethod
    public boolean loadFromJson(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.loadFromJson();
    }

    @Override
    @WebMethod
    public boolean removeJson(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.removeJson();
    }

    @Override
    @WebMethod
    public boolean saveToBinary(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.saveToBinary();
    }

    @Override
    @WebMethod
    public boolean loadFromBinary(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.loadFromBinary();
    }

    @Override
    @WebMethod
    public boolean removeBinary(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.removeBinary();
    }

    @Override
    @WebMethod
    public boolean saveToBase64(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.saveToBase64();
    }

    @Override
    @WebMethod
    public boolean loadFromBase64(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.loadFromBase64();
    }

    @Override
    @WebMethod
    public boolean removeBase64(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return domainService.removeBase64();
    }

}
